from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyappConfig(AppConfig):
    name = 'myapp'
    verbose_name = _("My App")
